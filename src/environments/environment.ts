// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // chatServiceUrl: 'http://localhost:8899',
  graphQlUrl: 'http://localhost:8899/graphql',
  // graphQlUrl: 'https://congdanso-uat.vnpost.vn/uat/vnpost-chat/graphql'
  chatServiceUrl: 'http://127.0.0.1:8882/vnpost_backend',
};


