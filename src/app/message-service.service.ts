import { environment } from './../environments/environment';
import { Injectable, Component } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';

declare var SockJS: any;
declare var Stomp: any;
declare var jQuery: any;


declare var connect: any;

@Injectable({
  providedIn: 'root'
})
export class MessageServiceService {

  public stompClient;
  public msg: string [] = [];

  public selectedUser: string;

  public userActive: string[] = [];

  lstUsers: string[] = [];

  constructor(private http: HttpClient) {

  }

  initializeWebSocketConnection(userName: string): Observable<any> {

    const serverUrl = `${environment.chatServiceUrl}/chat`;
    const ws = new SockJS(serverUrl);
    this.stompClient = Stomp.over(ws);
    const that = this;

    const params = new HttpParams().set('language', 'en');
    const body = JSON.stringify({});
    const headers = { 'content-type': 'text/plain' }

    return this.http.post<any>(`${environment.chatServiceUrl}/rest/user-connect`,
      body, { 'headers': headers, responseType: 'text' as "json", 'params': params })
      .pipe(
        map((data) => {
          console.log('data: ' + data);
          this.stompClient.connect({ username: userName }, function (frame) {
            console.log('connected...');
            that.stompClient.subscribe('/topic/broadcast', (message) => {
              // console.log('topic/broadcast', message)
              that.showMessage(JSON.parse(message.body).text)
            });
            that.stompClient.subscribe('/topic/active', () => {
              // console.log('vao topic active',data)
              that.getUserActive(userName).subscribe((data) => {
                console.log('data', data)
              });

            });
            that.stompClient.subscribe('/user/queue/messages', (message) => {
              // console.log('/user/queue/messages', message)
              that.showMessage(JSON.parse(message.body).text)
            });
            that.sendToBroadcast(userName, ' connected');
          })

        }),
        catchError((err) => {
          console.error(err);
          throw err;
        })
      )
  }

  connectWithMobile(usernameValue : string) {
    const that = this;
    const serverUrl = `${environment.chatServiceUrl}/chat`;
    const ws = new SockJS(serverUrl);
    this.stompClient = Stomp.over(ws);
    this.stompClient.connect({ username: usernameValue }, function (frame) {
      console.log('connected...')
      that.stompClient.subscribe('/user/queue/messages', (message) => {
        console.log('/user/queue/messages', message);
        that.showMessage(JSON.parse(message.body).text);
      });
    })
  }

  destroyWebsocketConnection(userName: string): Observable<any> {
    // if (this.stompClient != null) {
    //     this.stompClient.disconnect(function() {
    //       console.log('disconnected...');
    //   });
    const body = JSON.stringify({ 'username': userName });
    const headers = { 'content-type': 'text/plain' }

    return this.http.post<any>(`${environment.chatServiceUrl}/rest/user-disconnect`,
      body, { 'headers': headers, responseType: 'text' as "json" })
      .pipe(
        map((data) => {
          this.stompClient.disconnect({ username: userName }, function (frame) {
            console.log('disconnected...');
          });
        }),
        catchError((err) => {
          console.error(err);
          throw err;
        })
      )
  }


  showMessage(message: string) {
    console.log('message in service', message);
    this.msg.push(message);
  }


  getUserActive(userName: string): Observable<any> {

    var url = `${environment.chatServiceUrl}/rest/active-users-except/` + userName;
    return this.http.get<any>(url).pipe(
      map((data) => {

        console.log('data', data);
      })
    )
  }

  sendToBroadcast(username: string, message: string) {
    var text = username + message;
    this.stompClient.send("/app/broadcast-new", {}, JSON.stringify({ 'from': 'server', 'text': JSON.stringify({ 'from': 'server', 'text': text }) }));
  }

  // sendMessage(username: string, message: string) {
  //   this.selectedUser = '2';
  //   // this.stompClient.send('/app/broadcast-new', {'sender': null}, JSON.stringify({'from':null,  'text': message, 'recipient': null}));
  //   this.stompClient.send('/app/chat', { 'sender': null }, JSON.stringify({ 'from': null, 'text': message, 'recipient': this.selectedUser }));
  // }

  sendMessage(username: string, message: string, recipient: string, conversationId: string, time: string) {
    this.stompClient.send('/app/chat', { 'sender': username }, JSON.stringify({
      'from': username,
      'text': message,
      'recipient': recipient,
      'conversationId': conversationId,
      'time': time
    }));
  }
}
