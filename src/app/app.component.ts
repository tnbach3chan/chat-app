import { Component } from '@angular/core';

declare var jQuery:any;
declare var SockJS:any;
declare var Stomp:any;

declare var connect:any
declare var disconnect:any
declare var send:any
declare var clearMessages:any


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'chat-app';

  connect() {
    connect()
  }

  disconnect() {
    disconnect()
  }

  send() {
    send()
  }

  clearMessages() {
    clearMessages()
  }  
}
