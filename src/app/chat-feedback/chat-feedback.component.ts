import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { MessageServiceService } from './../message-service.service';
import { Component, OnInit } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';

declare var SockJS: any;
declare var Stomp: any;

// const FIND_PARTICIPANT = gql`
// query findAllParticipantInConversation($conversationId: String!) {
//   findAllParticipantInConversation(conversationId: $conversationId) {
//       username
//   }
// }
// `
// const FIND_CONVERSATION = gql`
// query {
//   findAllConversations {
//       name
//   }
// }
// `
const FIND_USER_CONVERSATION = gql`
query {
  getUserConversation(username: "tranduc1710", page: 0, size: 20) {
    id
    name
    avatarUrl
    numMember
    lastMessageAuthor
    lastMessage
    lastIndex
    tsUpdatedTime
    members
  }
}`;

// export class Conversation {
//   name: String
// }

// export class Participant {
// 	id: String
// 	userId: String
// 	username: string
// 	conversationId: String
// 	isAdmin: String
// }

// export class GetResponse {
//   findAllParticipantInConversation: Participant[];
//   findAllConversations: Conversation[];

// }
export class UserConversation {
  id?: string;
  name?: string;
  avatarUrl?: string;
  numMember?: string;
  lastMessageAuthor?: string;
  lastMessage?: string;
  lastIndex?: string;
  tsUpdatedTime?: string;
  members?: String[];
}

export class GetResponse {
  getUserConversation: UserConversation[];
}
@Component({
  selector: 'app-chat-feedback',
  templateUrl: './chat-feedback.component.html',
  styleUrls: ['./chat-feedback.component.css']
})
export class ChatFeedbackComponent implements OnInit {

  // findAllParticipantInConversation: Participant[] = [];
  // findAllConversations: Conversation[] = [];


  conversationId: String = "123"

  loading = true;

  input = '';
  username = '';
  
  public stompClient : any;

  constructor(
    public messageServiceService : MessageServiceService,
    private apollo: Apollo
  ) { }

  ngOnInit(): void {

    // this.appolo.watchQuery<GetResponse>({
    //   query: FIND_PARTICIPANT,
    //   variables: {
    //     conversationId: this.conversationId
    //   }
    // }).valueChanges
    // .subscribe(({data}) => {
    //   console.log(data.findAllParticipantInConversation);
    //   this.findAllParticipantInConversation = data.findAllParticipantInConversation;
    // })
    

    // this.appolo.watchQuery<GetResponse>({
    //   query: FIND_CONVERSATION
    // }).valueChanges
    // .subscribe(({data}) => {
    //   console.log(data.findAllConversations)
    //   this.findAllConversations = data.findAllConversations;
    // })
    this.apollo.watchQuery<GetResponse>({
      query: FIND_USER_CONVERSATION
    })
      .valueChanges
      .subscribe(({ data }) => {
        console.log(data.getUserConversation);
      });
 
  }


  connect() {
    this.messageServiceService.connectWithMobile(this.username);
  }


  disconnect() {
    this.messageServiceService.destroyWebsocketConnection(this.username).subscribe()
  }

  sendMessage() {


    // for (let participant of this.findAllParticipantInConversation) {
    //   console.log(participant.username)
    //   this.messageServiceService.sendMessage(this.username, this.input, participant.username)
    // }

    
    this.input='';
    console.log('current msg arr', this.messageServiceService.msg)
  }
 
}
